
const transformChiffreRomain = require('./RumanNumeral');
describe('transformer un nombre en chiffre Romain', () => {
    it('retourner I quand il y a 1 en entrée', () => {
        //Given
        const nombre = 1;

        //When
        const result = transformChiffreRomain(nombre);
        //Then
        expect(result).toEqual("I")
    })
    it('retourner II quand il y a 2 en entrée', () => {
        //Given
        const nombre = 2;

        //When
        const result = transformChiffreRomain(nombre);
        //Then
        expect(result).toEqual("II");
    })

    it('retourner III quand il y a 3 en entrée', () => {
        //Given
        const nombre = 3;

        //When
        const result = transformChiffreRomain(nombre);
        //Then
        expect(result).toEqual("III");
    })
    it('retourner IV quand il y a 4 en entrée', () => {
        //Given
        const nombre = 4;

        //When
        const result = transformChiffreRomain(nombre);
        //Then
        expect(result).toEqual("IV");
    })
    it('retourner V quand il y a 5 en entrée', () => {
        //Given
        const nombre = 5;

        //When
        const result = transformChiffreRomain(nombre);
        //Then
        expect(result).toEqual("V");
    })

})